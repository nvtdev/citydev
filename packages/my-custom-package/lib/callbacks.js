/*
Let's add a callback to the new post method that
appends a random emoji to the newly submitted post's title.
*/

import Telescope from 'meteor/nova:lib';

function PostsNewAddRandomEmoji (post, user) {

  post.title = post.title + " " +_.sample(["🎉", "💎", "☠", "⏱", "🎈", "⛱"])

  return post;
}
Telescope.callbacks.add("posts.new.sync", PostsNewAddRandomEmoji);

function PostsNewImageThumbnail (post, user) {

  if (post.thumbnailUrl == null) {
    post.thumbnailUrl = post.image;
  }

  if (post.image == null) {
    post.image = post.thumbnailUrl;
  }
  return post;
}
Telescope.callbacks.add("posts.new.sync", PostsNewImageThumbnail);

function PostsEditImageThumbnail (newPost, oldPost) {
  if (oldPost.thumbnailUrl == null) {
    //modifier.$set.thumbnailUrl = post.image;
    Posts.update(newPost._id, {$set:{
      thumbnailUrl: oldPost.image
    }});
  }

  if (oldPost.image == null) {
    //modifier.$set.thumbnailUrl = post.image;
    Posts.update(newPost._id, {$set:{
      image: oldPost.thumbnailUrl
    }});
  }
}
Telescope.callbacks.add("posts.edit.async", PostsEditImageThumbnail);
