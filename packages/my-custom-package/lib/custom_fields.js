import Posts from "meteor/nova:posts";
import Users from 'meteor/nova:users';
import Telescope from 'meteor/nova:lib';
import Upload from 'meteor/xavcz:nova-forms-upload';
import Tags from 'meteor/nova:forms-tags';

/*
Let's assign a color to each post (why? cause we want to, that's why).
We'll do that by adding a custom field to the Posts collection.
Note that this requires our custom package to depend on nova:posts and nova:users.
*/

// check if user can create a new post
const canInsert = user => Users.canDo(user, "posts.new");
// check if user can edit a post
const canEdit = Users.canEdit;

Posts.addField(
  {
    fieldName: 'color',
    fieldSchema: {
      type: String,
      control: "select", // use a select form control
      optional: true, // this field is not required
      insertableIf: canInsert,
      editableIf: canEdit,
      autoform: {
        options: function () { // options for the select form control
          return [
            {value: "white", label: "White"},
            {value: "yellow", label: "Yellow"},
            {value: "blue", label: "Blue"},
            {value: "red", label: "Red"},
            {value: "green", label: "Green"}
          ];
        }
      },
      publish: true // make that field public and send it to the client
    }
  }
);

Posts.addField({
  fieldName: 'image',
  fieldSchema: {
    type: String,
    optional: true,
    publish: true,
    control: Upload,
    insertableIf: canInsert,
    editableIf: canEdit,
    autoform: {
      options: {
        preset: Telescope.settings.get('cloudinaryPresets').posts // this setting refers to the transformation you want to apply to the image
      },
    }
  }
});

Posts.addField({
  fieldName: 'tags',
  fieldSchema: {
    type: [String],
    optional: true,
    publish: true,
    control: Tags,
    insertableIf: canInsert,
    editableIf: canEdit,
    autoform: {
      options: function () { // options for the select form control
        return [
          {value: "white", label: "White"},
          {value: "yellow", label: "Yellow"},
          {value: "blue", label: "Blue"},
          {value: "red", label: "Red"},
          {value: "green", label: "Green"}
        ];
      }
    }
  }
});

/*
The main post list view uses a special object to determine which fields to publish,
so we also add our new field to that object:
*/

import PublicationUtils from 'meteor/utilities:smart-publications';

PublicationUtils.addToFields(Posts.publishedFields.list, ["color"]);
PublicationUtils.addToFields(Posts.publishedFields.list, ["image"]);
PublicationUtils.addToFields(Posts.publishedFields.list, ["tags"]);
