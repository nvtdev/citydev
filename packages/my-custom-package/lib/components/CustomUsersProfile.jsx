import Telescope from 'meteor/nova:lib';
import React, { PropTypes, Component } from 'react';
import { FormattedMessage } from 'react-intl';
import { ListContainer } from "meteor/utilities:react-list-container";
import Posts from "meteor/nova:posts";
import Users from 'meteor/nova:users';
import { Link } from 'react-router';

const CustomUsersProfile = ({user}, {currentUser}) => {

  const twitterName = Users.getTwitterName(user);

  const terms = {view:"userPosts", userId: user._id};
  const {selector, options} = Posts.parameters.get(terms);

  return (
    <div className="page users-profile">
      <div className="users-profile-info">
        <Telescope.components.HeadTags url={Users.getProfileUrl(user, true)} title={Users.getDisplayName(user)} description={user.telescope.bio} />
        <h2 className="page-title"><Telescope.components.UsersAvatar user={user} size="xlarge"/>{Users.getDisplayName(user)}</h2>
        <p>{user.telescope.karma}</p>
        <p>{user.telescope.bio}</p>
        <ul>
          {twitterName ? <li><a href={"http://twitter.com/" + twitterName}>@{twitterName}</a></li> : null }
          {user.telescope.website ? <li><a href={user.telescope.website}>{user.telescope.website}</a></li> : null }
          <Telescope.components.CanDo document={user} action="users.edit">
            <li><Link to={Users.getEditUrl(user)}><FormattedMessage id="users.edit_account"/></Link></li>
          </Telescope.components.CanDo>
        </ul>
      </div>
      <h3><FormattedMessage id="users.posts"/></h3>
      <ListContainer
        collection={Posts}
        publication="posts.list"
        terms={terms}
        selector={selector}
        options={options}
        joins={Posts.getJoins()}
        cacheSubscription={false}
        component={Telescope.components.PostsList}
        componentProps={{showHeader: false}}
        listId="posts.list.user"
      />
    </div>
  )
}

CustomUsersProfile.propTypes = {
  user: React.PropTypes.object.isRequired,
}

CustomUsersProfile.contextTypes = {
  currentUser: React.PropTypes.object
}

CustomUsersProfile.displayName = "CustomUsersProfile";

module.exports = CustomUsersProfile;
