import Telescope from 'meteor/nova:lib';
import React, { PropTypes, Component } from 'react';
import { FormattedMessage, intlShape } from 'react-intl';
import { Button } from 'react-bootstrap';
import { Glyphicon } from 'react-bootstrap';
import { ModalTrigger } from "meteor/nova:core";

console.log("---------- Report Button ----------");

const CustomPostsReportButton = (props, context) => {

  const size = context.currentUser ? "large" : "small";
  const button = <Button id="bnt-report-post" className="posts-report-button" bsStyle="warning"><Glyphicon glyph="star"/><FormattedMessage id="posts.report"/></Button>;
  return (
      <ModalTrigger size={size} title={context.intl.formatMessage({id: "posts.report"})} component={button}>
        <Telescope.components.ReportForm/>
      </ModalTrigger>
  )
}

CustomPostsReportButton.displayName = "ReportButton";

CustomPostsReportButton.contextTypes = {
  currentUser: React.PropTypes.object,
  messages: React.PropTypes.object,
  intl: intlShape
}

module.exports = CustomPostsReportButton;
export default CustomPostsReportButton;
