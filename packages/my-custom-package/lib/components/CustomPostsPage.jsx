import Telescope from 'meteor/nova:lib';
import React, { PropTypes, Component } from 'react';
import Posts from "meteor/nova:posts";
import SocialShare from "meteor/nova:share";

const CustomPostsPage = ({document, currentUser}) => {

  const post = document;
  const htmlBody = {__html:  post.htmlBody};

  console.log(post.tags);

  return (
    <div className="posts-page">
      {(currentUser && currentUser.isAdmin) ? <Telescope.components.PostsItem post={post}/> : null}
      <div className="row">
        <div className="col-md-6">
          <Telescope.components.HeadTags url={Posts.getLink(post)} title={post.title} image={post.thumbnailUrl} />
          {/*<Telescope.components.PostsItem post={post}/>*/}
          <h2> {post.title} </h2>
          {post.htmlBody ? <div className="posts-page-body" dangerouslySetInnerHTML={htmlBody}></div> : null}

          {/*<SocialShare url={ Posts.getLink(post) } title={ post.title }/>*/}
        </div>
        <div className="col-md-6">
          <img className="image" src={post.image}/>
        </div>
      </div>
      <div className="row">
        <div className="btn-post-options">
          <Telescope.components.ReportButton/>
          <Telescope.components.SocialShare/>
        </div>
      </div>
      
      <Telescope.components.PostsCommentsThread document={post} currentUser={currentUser}/>

    </div>
  )
};

CustomPostsPage.displayName = "CustomPostsPage";

module.exports = CustomPostsPage;
