import Telescope from 'meteor/nova:lib';
import React, { PropTypes, Component } from 'react';
import { intlShape } from 'react-intl';
import NovaForm from "meteor/nova:forms";
import { withRouter } from 'react-router'
import Posts from "meteor/nova:posts";
import Formsy from 'formsy-react';
import { Input } from 'formsy-react-components';
import { Button } from 'react-bootstrap';
import NovaEmail from 'meteor/nova:email';
import Users from "meteor/nova:users";

const CustomPostsReportForm = (props, context) => {

  const router = props.router;
  const post = document;
  // const htmlBody = {__html:  post.htmlBody};

  const sendReport = (data) => {

    console.log(data.reason);
    const adminEmail = Users.findOne({isAdmin: true}).emails[0].address;
    const email = NovaEmail.emails['customReport'];
    const properties = email.getProperties(data);
    const subject = email.subject(properties);
    const html = NovaEmail.getTemplate(email.template)(properties);
    NovaEmail.buildAndSendHTML(adminEmail, subject, html);
  }

  return (
    <Telescope.components.CanDo
      action="posts.report"
      noPermissionMessage="users.cannot_report"
      displayNoPermissionMessage={true}
    >
    <div className="posts-report-form">
      <Formsy.Form className="report-form" onSubmit={sendReport} >
        <div className="form-group">
          <label for="comment">Reason:</label>
            <Input
              name="reason"
              type="textarea"
              layout="elementOnly"
            />
          <Button className="report-button" type="submit" bsStyle="primary">Send report</Button>
        </div>
      </Formsy.Form>
    </div>
    </Telescope.components.CanDo>
  )

}

CustomPostsReportForm.contextTypes = {
  currentUser: React.PropTypes.object,
  messages: React.PropTypes.object,
  intl: intlShape
};

CustomPostsReportForm.displayName = "CustomPostsReportForm";

module.exports = withRouter(CustomPostsReportForm);
export default withRouter(CustomPostsReportForm);
