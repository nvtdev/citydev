import Telescope from 'meteor/nova:lib';
import React from 'react';

const CustomPostsList = ({results, currentUser, hasMore, ready, count, totalCount, loadMore, showHeader = true}) => {

  let StickyPosts = [];
  let OtherPosts = [];

  for (var i=0; i < results.length; i++) {
    if (results[i].sticky) {
      StickyPosts.push(results[i]);
    } else {
      OtherPosts.push(results[i]);
    }
  }

  if (!!results.length) {
    return (
      <div className="posts-list">
        {showHeader ? <Telescope.components.PostsListHeader /> : null}
        <div className="posts-list-content-sticky">
          {StickyPosts.map(post => <Telescope.components.PostsItem post={post} currentUser={currentUser} key={post._id}/>)}
        </div>
        <div className="posts-list-content">
          {OtherPosts.map(post => <Telescope.components.PostsItem post={post} currentUser={currentUser} key={post._id}/>)}
        </div>
        {hasMore ? (ready ? <Telescope.components.PostsLoadMore loadMore={loadMore} count={count} totalCount={totalCount} /> : <Telescope.components.PostsLoading/>) : <Telescope.components.PostsNoMore/>}
      </div>
    )
  } else if (!ready) {
    return (
      <div className="posts-list">
        {showHeader ? <Telescope.components.PostsListHeader /> : null}
        <div className="posts-list-content">
          <Telescope.components.PostsLoading/>
        </div>
      </div>
    )
  } else {
    return (
      <div className="posts-list">
        {showHeader ? <Telescope.components.PostsListHeader /> : null}
        <div className="posts-list-content">
          <Telescope.components.PostsNoResults/>
        </div>
      </div>
    )
  }

  };

  CustomPostsList.displayName = "CustomPostsList";

  module.exports = CustomPostsList;
