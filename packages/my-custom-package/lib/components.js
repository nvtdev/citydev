/*
This file centralizes all our custom component overrides.
*/

import Telescope from 'meteor/nova:lib';

import CustomLogo from "./components/CustomLogo.jsx";
import CustomNewsletter from "./components/CustomNewsletter.jsx";
import CustomPostsItem from "./components/CustomPostsItem.jsx";
import CustomPostsList from "./components/CustomPostsList.jsx";
import CustomPostsPage from "./components/CustomPostsPage.jsx";
import CustomUsersProfile from "./components/CustomUsersProfile.jsx";
import CustomPostsReportButton from "./components/CustomPostsReportButton.jsx";
import CustomPostsReportForm from "./components/CustomPostsReportForm.jsx";

Telescope.components.Logo = CustomLogo;
Telescope.components.Newsletter = CustomNewsletter;
Telescope.components.PostsItem = CustomPostsItem;
Telescope.components.PostsList = CustomPostsList;
//Telescope.components.PostsPage = CustomPostsPage;
Telescope.components.UsersProfile = CustomUsersProfile;
Telescope.components.ReportButton = CustomPostsReportButton;
Telescope.components.ReportForm = CustomPostsReportForm;

// Telescope.registerComponent("PostsList",require('./components/CustomPostsList.jsx'));
