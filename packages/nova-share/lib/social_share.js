import Telescope from 'meteor/nova:lib';

Telescope.registerComponent("SocialShare", require('./common/social_share.jsx'));
