import React, { PropTypes, Component } from 'react';
import Telescope from 'meteor/nova:lib';

class SocialShare extends Component {

  constructor() {
    super();
    this.toggleView = this.toggleView.bind(this);
    this.state = {
      showShare: false
    }
  }

  viaTwitter() {
    return !!Settings.get('twitterAccount') ? 'via='+Settings.get('twitterAccount') : '';
  }

  toggleView() {
    this.setState({
      showShare: !this.state.showShare
    });
    return;
  }

  insertIcon(name) {
    return {__html: Telescope.utils.getIcon(name)};
  }

  render() {
    let shareDisplay = this.state.showShare ? 'active' : 'hidden';
    let siteUrl = "localhost:8080/";
    //let shareUrl = siteUrl + this.props.url; {/* hard coding our domain url for now should fix later */}
    let shareUrl = window.location.href;

    return (
      <div className="social-share posts-item-meta">
        <a className="share-link action" title="share" onClick= { this.toggleView} href="#">Share</a>
        <div className={ `share-options ${shareDisplay}` }>
          <a className="share-option-facebook" href={ `//www.facebook.com/sharer/sharer.php?u=${ encodeURIComponent(shareUrl) }`} target="_blank" dangerouslySetInnerHTML={ this.insertIcon('facebook')} />
          <a className="share-option-twitter" href={ `//twitter.com/intent/tweet?text=${ encodeURIComponent(shareUrl) }` } target="_blank" dangerouslySetInnerHTML={ this.insertIcon('twitter')}/>
          <a className="share-option-linkedin" href={ `//www.linkedin.com/shareArticle?mini=true&url=${ encodeURIComponent(shareUrl) }&summary=${ encodeURIComponent(this.props.title) }` } target="_blank" dangerouslySetInnerHTML={ this.insertIcon('linkedin')}/>
        </div>
      </div>
    )
  }
}

SocialShare.propTypes = {
  url: React.PropTypes.string.isRequired,
  title: React.PropTypes.string.isRequired,
}

module.exports = SocialShare;
